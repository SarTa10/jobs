package com.example.ug_quizjobs.repositories;
import com.example.ug_quizjobs.jpa.entities.UserEntity;
import com.example.ug_quizjobs.jpa.entities.statistic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
public interface statisticRepository extends JpaRepository<statistic, Long>, JpaSpecificationExecutor<statistic>  {

}

