package com.example.ug_quizjobs.services;

import com.example.ug_quizjobs.jpa.entities.statistic;
import com.example.ug_quizjobs.repositories.UserRepository;
import com.example.ug_quizjobs.repositories.statisticRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class StatisticService {

    private final UserRepository userRepository;

    private final statisticRepository statisticRepository;

    @Autowired
    public StatisticService(UserRepository userRepository, statisticRepository statisticRepository) {
        this.userRepository = userRepository;
        this.statisticRepository = statisticRepository;
    }

    public Long addStatistic() {
        Long count = this.userRepository.count();

        statistic statistic = new statistic();
        statistic.setDate(new Date());
        statistic.setUsers(count);

        statisticRepository.save(statistic);

        return count;
    }
}
