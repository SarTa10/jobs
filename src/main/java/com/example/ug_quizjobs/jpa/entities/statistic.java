package com.example.ug_quizjobs.jpa.entities;


import com.example.ug_quizjobs.dto.UserDto;
import lombok.*;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@EqualsAndHashCode()
@Table(name = "STATISTIC")
public class statistic {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "userIdSeq", sequenceName = "USER_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "userIdSeq")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE", updatable = false)
    private Date date;

    @Column(name = "USERS")
    private Long users;

}
