package com.example.ug_quizjobs.dto;

import lombok.Data;

@Data
public class UserDto {
    private Long userId;
    private String userEmail;
    private String userPassword;
}
