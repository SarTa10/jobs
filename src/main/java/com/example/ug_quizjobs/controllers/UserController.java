package com.example.ug_quizjobs.controllers;

import com.example.ug_quizjobs.dto.ApiResponse;
import com.example.ug_quizjobs.dto.UserDto;
import com.example.ug_quizjobs.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {
    private final UserService departmentService;

    @Autowired
    public UserController(UserService departmentService) {
        this.departmentService = departmentService;
    }

    @PostMapping("/user/add")
    public ApiResponse addDepartment(@RequestBody UserDto userDto) {
        return departmentService.addUser(userDto);
    }
}
