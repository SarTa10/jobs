package com.example.ug_quizjobs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableJpaAuditing
@EnableScheduling
public class UgQuizSpringExceptionsApplication {

    public static void main(String[] args) {
        SpringApplication.run(UgQuizSpringExceptionsApplication.class, args);
    }

}
