package com.example.ug_quizjobs.apiUtils;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.IM_USED)
public class UserAlreadyExistsException extends RuntimeException {
    private String usedEmail;

    public UserAlreadyExistsException() {
        super("USER_ALREADY_EXISTS_EXCEPTION");
    }

    public UserAlreadyExistsException setProblemEmail(String email) {
        this.usedEmail = email;
        return this;
    }
}
