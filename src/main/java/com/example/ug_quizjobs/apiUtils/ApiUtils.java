package com.example.ug_quizjobs.apiUtils;

import com.example.ug_quizjobs.dto.ApiResponse;

public class ApiUtils {

    public static ApiResponse getApiResponse(Object object) {

        String name  = object.getClass().getSimpleName();
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.addData(name, object);
        return apiResponse;

    }
}
