package com.example.ug_quizjobs.components;

import com.example.ug_quizjobs.jpa.entities.statistic;
import com.example.ug_quizjobs.repositories.UserRepository;
import com.example.ug_quizjobs.services.StatisticsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

@Component
public class userScheduler {


    public static final Logger logger = LoggerFactory.getLogger(userScheduler.class);

    public static final SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm:ss");

    private final StatisticsService statisticsService;

    @Autowired
    public userScheduler(StatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }

    @Scheduled(fixedDelay = 1, timeUnit = TimeUnit.HOURS)
    public void saveEmployeeStats() {
        Long empCount = statisticsService.addStatisticsEntry();

        logger.info("We have {} employees", empCount);
    }

}


